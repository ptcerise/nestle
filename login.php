<?php
$fname = $email = $lname = $phone = $birthday = $id = "";

if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST["id"])) {
    $id = check($_POST["id"]);
    $email = check($_POST["email"]);
    $fname = check($_POST["first_name"]);
    $lname = check($_POST["last_name"]);
    $phone = check($_POST["phone"]);
    //$birth = explode('/', check($_POST["birthday"]));
    //$birthday = $birth[1] . '/' . $birth[0] . '/' . $birth[2];
    create_edit_user($email,$id, $fname, $lname, $phone);
}else{
    $email = check($_POST["email"]);
    get_user($email);
}

/**
 * Sanitize the input
 * @param $data
 * @return string
 */
function check($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);

    return $data;
}

/**
 * Get user data
 * @param $email
 */
function get_user($email)
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://api.sendinblue.com/v2.0/user/".$email);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
    $headers = array();
    $headers[] = "api-key:jHrCQcUzv1D3IA4L";
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    $result = curl_exec($ch);
    if (curl_errno($ch)) {
        echo 'Error:' . curl_error($ch);
    }
    curl_close($ch);
    echo $result;
}


/**
 * Create or Update user data
 * @param $email
 * @param $id
 * @param $fname
 * @param $lname
 * @param $phone
 * @param $birthday
 */
function create_edit_user($email, $id, $fname, $lname, $phone)
{
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, "https://api.sendinblue.com/v2.0/user/createdituser");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,
        "{\"email\":\"" . $email . "\",\"attributes\":{\"FACEBOOK_ID\":\"" . $id . "\",\"FIRST_NAME\":\"" . $fname . "\",\"LAST_NAME\":\"" . $lname . "\",\"PHONE\":\"" . $phone . "\"},\"listid\":[39]}");
    curl_setopt($ch, CURLOPT_POST, 1);

    $headers = array();
    $headers[] = "api-key:jHrCQcUzv1D3IA4L";
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    $result = curl_exec($ch);
    if (curl_errno($ch)) {
        echo 'Error:' . curl_error($ch);
    }
    curl_close($ch);
    echo $result;
}

